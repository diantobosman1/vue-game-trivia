# Assignment 2 - Vue 3 - Trivia Game

[![web](https://img.shields.io/static/v1?logo=heroku&message=Online&label=Heroku&color=430098)](https://vue-game-trivia.herokuapp.com/)

## Assignment
This application is a trivia game as a Single Page Application using the Vue.js framework (version 3.x). This project is part of Noroff Frontend. The assignment requires knowlegde of the js framework's fundamentals, specifically Vue.js, which covers fetching an API, dealing with states, props, and lifecicle methods. 

## Use the app click on the link bellow
https://vue-game-trivia.herokuapp.com/

## How to play
1- Enter your name
2- Choose the number of questions you want to answer
3- Choose the difficulty and category as wall
4- Click on "Let's go" to start
5- Choose an answers for each question and click on the pop menu to confirm
6- Always click on the "next question"
7- If you have answered all the questions, click on "Go to the results"button

## Design Component tree
https://www.figma.com/file/rXMR6kB9G0KXsTT55Gsqju/Component-Tree-Trivia-Vue-API?node-id=0%3A1

## Contributing
I am open to receive advice and tips which can help me to improve the design, the logic, to clean up the code, etc. So that I can improve myself as a developer.

## Built With
[Microsoft VSCode](https://code.visualstudio.com/)

## Technologies
- Vue 3
- JavaScript
- HTML
- Tailwind CSS

## Author
[Dianto Bosman](https://gitlab.com/diantobosman1)
[Igor Figueiredo](https://gitlab.com/Igor-GF)

## Acknowledgment
[Dean von Schoultz](https://gitlab.com/deanvons) who is the tutor of the Frontend course at Noroff.

## Project status
Project exclusively made for study purposes.

## License
[MIT](https://choosealicense.com/licenses/mit/)
