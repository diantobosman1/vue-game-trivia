import Start from "../views/StartView.vue";
import Questions from "../views/QuestionView.vue";
import Results from "../views/ResultView.vue";
import { createRouter, createWebHistory } from "vue-router";

const routes = [
    {
      path: "/",
      component: Start,
    },
    {
      path: "/questions",
      component: Questions,
    }
    ,
    {
      path: "/results/",
      component: Results,
    }
  ];

export default createRouter({
    history: createWebHistory(),
    routes
})