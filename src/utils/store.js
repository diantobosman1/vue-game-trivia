import { createStore } from "vuex";

const store = createStore({
    state:{
        name: "",
        id: [],
        highscore: "",
        questionURL: "",
        questions: [],
        answeredquestions: []
    },

    mutations:{
        setQuestions: (state, payload) => {
            state.questions = payload
        },
        addAnsweredQuestion: (state, load) => {
            state.answeredquestions.push({load})
        },
        setQuestionURL: (state, payload) => {
            state.questionURL = payload
        },
        setName: (state, payload) => {
            state.name = payload
        },
        setHighScore: (state, payload) => {
            state.highscore = payload
        },
        setId: (state, payload) => {
            state.id = payload
        }
    },

    actions:{
        async getQuestions({ commit}) {
            let URL = store.state.questionURL
            const response = await fetch(URL)
            const questions = await response.json()
            commit("setQuestions", questions.results)
        }
    }
})

export default store;