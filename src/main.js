import { createApp } from 'vue'
import App from './App.vue'
import store from './utils/store'
import router from './utils/router'
import './index.css'

createApp(App)
.use(router)
.use(store)
.mount('#app')