//-- Import the url and key from the index file
import { apiURL } from ".";
import { apiKey } from ".";
import store from "../utils/store";

let currentScore = 0

//-- Check if user exists
export function checkName(username) {
    fetch(`${apiURL}/trivia?username=${username}`)
        .then(response => response.json())
        .then(results => {

            //-- Check if the user exists. If not, create new user. Otherwise, login.
            if(results.length == 0) {
                return apiUserRegister(username);
                
            } else {
                attemptLogin(username);
                alert("User was already registered. You are logged in now.");
            }
        })
        .catch(error => {
            console.log(error.message);
        })
}

// POST : create a new user
export function apiUserRegister(username) {
    fetch(`${apiURL}/trivia`, {
        method: 'POST',
        headers: {
            'X-API-Key': apiKey,
        'Content-Type': 'application/json'
        },
        body: JSON.stringify({ 
            username, 
            highScore: 0 
        })
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Could not create new user')
        }
        return response.json()
    })
    .then(newUser => {
        //-- Commit the new user to the store
        store.commit("setName", newUser.username)
        store.commit("setHighScore", newUser.highScore)
        store.commit("setId", newUser.id)
        return newUser;
    })
    .catch(error => {
        console.log(error)
    })
}

//-- Change the highscore of the user
export function updateHighscore(userId) {
    //-- Check if the score is higher than the current highscore
    if (store.state.highscore > currentScore) {

    fetch(`${apiURL}/trivia/${userId}`, {
            method: 'PATCH', // NB: Set method to PATCH
            headers: {
                'X-API-Key': apiKey,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                // Provide new highScore to add to user with id
                highScore: store.state.highscore 
            })
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Could not update high score')
            }
            return response.json()
        })
        .then(updatedUser => {
            currentScore = updatedUser.highScore
        })
        .catch(error => {
            console.log(error)
        })
    }
    else {
        alert("The highscore is lower than your personal record.")
    }
}

//-- Function to login the user
function attemptLogin(name) {
    fetch(`${apiURL}/trivia?username=${name}`)
      .then((response) => response.json())
      .then((results) => {
          //-- Check if the user exists
        if (results.length == 0) {
            console.log("The user does not exists")
        } 
        else {
            //-- Login the user
            store.commit("setName", results[0].username)
            store.commit("setHighScore", results[0].highScore)
            store.commit("setId", results[0].id)
        }
      })
      .catch((error) => {
          console.log(error)
      });
  }